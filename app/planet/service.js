const Planet = require('./model');
const swapi = require('../shared/service/swapi')
//const errorHandler = require('../common/errorHandler');


Planet.methods(['get', 'post', 'put', 'delete']);
Planet.updateOptions({new:true});
//Planet.after('post', errorHandler).after('put', errorHandler);



Planet.before('post', function(req, res, next) {
  swapi('https://swapi.co/api/planets/?search=' + req.body.name ).then(function (result) {
      console.log(result);
      
      if (result !== undefined) {
          //req.body['terrain'] = result['terrain'];
          //req.body['climate'] = result['climate'];
          req.body['movie_appearances'] = result['films'].length;
      }
      next();
  });
});

module.exports = Planet;