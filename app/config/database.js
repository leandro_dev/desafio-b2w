const mongoose = require('mongoose');
mongoose.Promise = global.Promise

const host = "db";
const port = "27017";

const options = {
    useCreateIndex: true,
    useNewUrlParser: true
 };

module.exports = mongoose.connect(`mongodb://${host}:${port}/starWars`, options);