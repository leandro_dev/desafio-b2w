FROM node:carbon

WORKDIR /usr/src/app

COPY package*.json /usr/src/
COPY loader.js /usr/src/

RUN npm install

COPY ./app /usr/src/app

EXPOSE 3000

CMD [ "npm", "start" ]