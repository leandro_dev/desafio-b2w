const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

module.exports = (server) => {

    const router = express.Router();
    server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    server.use('/api', router);
    server.use(function(req, res) {
        res.redirect('/docs');
    });


    const planetService = require('../planet/service');
    planetService.register(router, '/planets');

}