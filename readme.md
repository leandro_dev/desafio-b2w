# Desafio B2W

A aplicação é uma API REST para cadastro de planetas do universo cinematográfico de Star Wars


## Tecnologias Utilizadas
* Node
* JavaScript
* Mongodb
* Docker

## Pré-requisitos

- Docker >= 18.09.7
- Git >= 2.17.1

## Setup

1. Clone o repositório com "https://leandro_dev@bitbucket.org/leandro_dev/desafio-b2w.git"
2. Mude para o diretório do projeto com o comando "cd desafio-b2w"
3. Execute o comando "docker-compose up"


## Teste dos endpoints

Criei uma documentação utilizando o swagger para facilitar os testes.

Link: localhost:3000/docs