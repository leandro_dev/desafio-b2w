const port = 3000;

const bodyParser = require('body-parser');
const express = require('express');
const morgan = require('morgan');
const server = express();

server.use(morgan('dev'));
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());
server.use(logErrors);

function logErrors(err, req, res, next) {
    console.error(err.stack);
    next(err);
  }
  

server.listen(process.env.port || port, function (){
    console.log('linsten on - APP --s');
});

module.exports = server;