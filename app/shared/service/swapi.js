const https = require('https');

var swapi = function (url) {
    return new Promise(function (resolve, reject) {
        https.get(url, function (resp) {
            var data = '';
            resp.on('data', function (chunk) {
                data += chunk;
            });
            resp.on('end', function () {
                var result = JSON.parse(data)['results'][0];
                resolve(result)
            });
        }).on("error", function(err){
            reject(err)
        });
    });

}


module.exports = swapi;