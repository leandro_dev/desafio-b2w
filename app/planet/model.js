const restful = require('node-restful');
const mongoose = restful.mongoose;

const PlanetSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Por favor, digite o nome:'],
        unique: [true, 'Já existe um planeta com esse nome.']
    },
    climate: {
        type: String,
        required: [true, 'Por favor, digite o tipo de clima:']
    },
    terrain: {
        type: String,
        required: [true, 'Por favor, digite o tipo do terreno:']
    },
    movie_appearances: {
        type: String,
        default: 0
    }
}
, { versionKey: false });

module.exports = restful.model('Planets', PlanetSchema);